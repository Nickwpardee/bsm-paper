# knitr slides

The knitr slides are based on mtheme <https://github.com/matze/mtheme>. Refer to mtheme's documentation for building the `.sty` files.

## Modifications

The color scheme has been modified to fit in with the University of Wyoming's brand colors.

Modify `beamercolorthememetropolis.sty` by adding the lines

```
\definecolor{uwyoGold}{HTML}{FFC425}
\definecolor{uwyoBrown}{HTML}{492F24}
```

and by modifying the associated lines:

```
\newcommand{\@metropolis@colors@light}{
  \setbeamercolor{normal text}{%
    fg=uwyoBrown,
    bg=black!2
  }
}
\setbeamercolor{alerted text}{%
  fg=uwyoGold
}
```

## Compiling to PDF

1. knitr
2. LuaLaTeX
3. bibTeX
4. LuaLaTeX
