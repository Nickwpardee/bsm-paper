\documentclass[11pt]{beamer}\usepackage[]{graphicx}\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}

\usetheme{m}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}
\usepackage{subcaption}  % an alternative package for sub figures
\newcommand{\subfloat}[2][need a sub-caption]{\subcaptionbox{#1}{#2}}
\usepackage{graphicx}
\usepackage[final]{microtype}

\title{{\normalfont \bfseries bsm}}
\subtitle{An R Package for Bayesian Survival Modeling}
\date{\today}
\author{Brett Klamer}
\institute{University of Wyoming}
\titlegraphic{\hfill\includegraphics[height=1.5cm]{ext-fig/logo.png}}
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}



\maketitle

\begin{frame}
  \frametitle{Table of Contents}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents[hideallsubsections]
\end{frame}

%====================================================================
% Section 1
%====================================================================
\section{Survival Analysis}

%====================================================================
% Frame 1.1
%====================================================================
\begin{frame}[fragile]
  \frametitle{What is survival analysis?}
  Models relationship between covariates and event using time. \pause

  \begin{itemize}
    \item Time
  \begin{itemize}
  \item The number of (hours, days, months, years) between two time points. \pause
  \item Each subject is assumed to start at the time $t=0$ point. \pause
  \end{itemize}
  \item Event
  \begin{itemize}
  \item Any change in a subject's status. \pause
  \item Death, disease, equipment failure, birth, remission, etc. \pause
  \end{itemize}
  \item Model
  \begin{itemize}
  \item $Y \sim X$
  \end{itemize}
  \end{itemize}
\end{frame}

%====================================================================
% Frame 1.2
%====================================================================
\begin{frame}[fragile]
  \frametitle{What is special about survival analysis?}
  \onslide<1-> When the event time is unknown.
  \begin{itemize}
  \onslide<2-> \item Right-censoring
  \begin{itemize}
  \onslide<3-> \item Subject never experiences event during study time.
  \end{itemize}
  \onslide<2-> \item Left-censoring
  \begin{itemize}
  \onslide<4-> \item Subject experienced event before first measurement.
  \end{itemize}
  \onslide<2-> \item Interval-censoring
  \begin{itemize}
  \onslide<5-> \item Subject experiences event between two known time points.
  \onslide<6-> \item Occurs with periodic follow ups.
  \end{itemize}
  \end{itemize}
\end{frame}

%====================================================================
% Frame 1.3
%====================================================================
\begin{frame}[fragile]
  \frametitle{What is special about survival analysis?}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}[ht]

{\centering \includegraphics[width=\maxwidth]{figure/right-censor-graphic-1} 

}

\caption[Example of Right Censoring]{Example of Right Censoring}\label{fig:right-censor-graphic}
\end{figure}


\end{knitrout}
\end{frame}

%====================================================================
% Frame 1.4
%====================================================================
\begin{frame}[fragile]
  \frametitle{What is special about survival analysis?}
  \onslide<1-> When covariates change over time.
  \begin{itemize}
  \onslide<2-> \item Internal time-dependent covariate
  \begin{itemize}
  \onslide<3-> \item Variability in measurement is based on characteristics or behavior of subject.
  \onslide<4-> \item Blood pressure, disease complications, etc.
  \end{itemize}
  \onslide<2-> \item External time-dependent covariate
  \begin{itemize}
  \onslide<5-> \item Variability in measurement is based on outside forces and may affect all subjects.
  \onslide<6-> \item Environmental conditions, age, etc.
  \end{itemize}
  \end{itemize}
\end{frame}

%====================================================================
% Frame 1.5
%====================================================================
\begin{frame}[fragile]
  \frametitle{Basic methods}
  Survival function: $S(t) = P(T > t)$
  
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}

{\centering \includegraphics[width=\maxwidth]{figure/theoretical_survival_curve-1} 

}

\caption[Theoretical graph of \(S(t)\)]{Theoretical graph of \(S(t)\)}\label{fig:theoretical_survival_curve}
\end{figure}


\end{knitrout}
  
\end{frame}

%====================================================================
% Frame 1.6
%====================================================================
\begin{frame}[fragile]
  \frametitle{Basic Methods}
  Hazard Function
  \begin{equation}
  \nonumber
  \begin{aligned}
  h(t) &= -\frac{d}{dt} \log S(t) \\
  H(t) &= - \log S(t)
  \end{aligned}
  \end{equation}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}
\subfloat[Theoretical Hazard Curves\label{fig:theoretical-hazard-graphs1}]{\includegraphics[width=.49\linewidth]{figure/theoretical-hazard-graphs-1} }
\subfloat[Theoretical Cumulative Hazard Curve\label{fig:theoretical-hazard-graphs2}]{\includegraphics[width=.49\linewidth]{figure/theoretical-hazard-graphs-2} }\caption[Types of Hazard Curves]{Types of Hazard Curves}\label{fig:theoretical-hazard-graphs}
\end{figure}


\end{knitrout}

\end{frame}

%====================================================================
% Frame 1.7
%====================================================================
\begin{frame}[fragile]
  \frametitle{Regression Model}
  Cox Proportional Hazards Regression Model \cite{cox1972}
  \begin{equation}
  \nonumber
  \begin{aligned}
  h_i(t;\mathbf{X}) &= h_0(t)e^{\sum_{j=1}^p(\beta_{j}X_{ij})}
  \end{aligned}
  \end{equation}
  \pause
  \begin{itemize}
  \item Assumes proportional hazards \pause
  \item For right-censored data \pause
  \item Leaves $h_0(t)$ unspecified through use of partial likelihood \pause
  \item Inference is focused on $e^{\beta} = \textnormal{Hazard Ratio}$ \pause
    \item Extensions accommodate time-dependent covariates
  \end{itemize}
\end{frame}

%====================================================================
% Section 2
%====================================================================
\section{Overview of Survival Packages in R}

%====================================================================
% Frame 2.1
%====================================================================
\begin{frame}[fragile]
  \frametitle{What is R?}
  R is an interpreted, dynamic, functional language with object oriented data structures and methods. \cite{morandat2012, wickham2011} \pause
  \begin{itemize}
  \item Package - Collection of documented functions and data that can be shared with others. \pause
  \item Library - Directory of packages.
  \end{itemize}
\end{frame}

%====================================================================
% Frame 2.2
%====================================================================
\begin{frame}[fragile]
  \frametitle{``Survival'' package in R}
  \textbf{survival} package by Therneau \cite{survival} \pause
  \begin{itemize}
  \item Classical inference \pause
  \item The defining survival analysis package \pause
  \item Provides methods for beginning to end of an analysis \pause
  \item What \texttt{lm()} is to regression, \textbf{survival} is to survival analysis
  \end{itemize}
\end{frame}

%====================================================================
% Frame 2.3
%====================================================================
\begin{frame}[fragile]
  \frametitle{Other Classical Inference Packages}
coxphf, coxphw, coxrobust, CPHshape, eha, Icsurv, intcox, mfp, MIICD, multcomp, multipleNCC, NestedCohort, OrdFacReg, paf, rms, smoothHR, survey, survivalMPL, timereg, etc.

\end{frame}

%====================================================================
% Frame 2.4
%====================================================================
\begin{frame}[fragile]
  \frametitle{Bayesian packages}
  No Bayesian equivalent of the \textbf{survival} package
  \begin{itemize} \pause
  \item Enter data how? \pause
  \item Survival model? \pause
  \item Model selection? \pause
  \item Sampling? \pause
  \end{itemize}
  BaSTA, bayesSurv, BayHaz, BGPhazard, BMA, bsm, DPpackage, dynsurv, IDPSurvival, LearnBayes, mixAK, MRH, psbcGroup, SemiCompRisks, spatsurv, spBayesSurv, MCMCglmm, MCMCpack, PReMiuM, etc.
\end{frame}

%====================================================================
% Frame 2.5
%====================================================================
\begin{frame}[fragile]
  \frametitle{DPpackage vs. bsm}
  \textbf{DPpackage} offers modeling with time-dependent covariates, random effects, model selection, and interval censoring. \cite{dppackage} \pause
  \begin{itemize}
  \item Doesn't use a \textbf{survival}-like syntax \pause
  \item Complicated models \pause
  \begin{itemize}
  \item Generalized linear mixed models with Dirichlet process mixtures
  \end{itemize} \pause
  \item 268 page manual with 59 functions
  \end{itemize}
\end{frame}

%====================================================================
% Section 3
%====================================================================
\section{bsm}

%====================================================================
% Frame 3.1
%====================================================================
\begin{frame}[fragile]
  \frametitle{Package Framework}

\begin{itemize}
\item \textit{Writing R Extensions} by the R Core Team \cite{r-ext}
\item \textit{R Packages} by Wickham \cite{wickham2015}
\item devtools package by Wickham and Chang \cite{devtools}
\item roxygen2 package by Wickham, et al. \cite{roxygen2}
\item testthat package by Wickham \cite{testthat}
\item rmarkdown package by Xie, et al. \cite{rmarkdown}
\end{itemize}

\end{frame}

%====================================================================
% Frame 3.2
%====================================================================
\begin{frame}[fragile]
  \frametitle{bsm}

\begin{figure}
\includegraphics[scale=1]{ext-fig/bsm-folder.pdf} 
\end{figure}

\end{frame}

%====================================================================
% Frame 3.3
%====================================================================
\begin{frame}[fragile]
  \frametitle{bsm}
  \textbf{bsm} offers survival modeling with time-dependent covariates, random effects, model selection, and interval censoring. \pause
  \begin{itemize}
  \item \textbf{survival}-like syntax \pause
  \item Uses common Gamma process Cox model \pause
  \item Default weakly informative priors \pause
  \item Sampling through JAGS or Stan
  \end{itemize}
\end{frame}

%====================================================================
% Frame 3.4
%====================================================================
\begin{frame}[fragile]
  \frametitle{Counting process model}
  Cox-type counting process model \cite{aalen1975}
  \begin{equation}
  \nonumber
  \begin{aligned}
  \lambda_i(t;\mathbf{X}(t)) &= Y_i(t) h_0(t)e^{\sum_{j=1}^p(\beta_{j}X_{ij}(t))} \\ 
  D &= \{ K_i(t), Y_i(t), x_i(t); i = 1,\ldots, n\}
  \end{aligned}
  \end{equation}
  \pause
  \begin{itemize}
  \item Right-censored data \pause
  \item Time-dependent covariates \pause
  \item Using Bayesian framework so specify $H_0(t)$ as a step process \pause
  \item Possible to accommodate time-dependent strata, left truncation, multiple events per subject
  \end{itemize}
\end{frame}

%====================================================================
% Frame 3.5
%====================================================================
\begin{frame}[fragile]
  \frametitle{Bayesian Inference}
  \begin{equation}
  \nonumber
\label{propto-likelihood}
\begin{aligned}
L_i(\beta,\mathop{\textnormal{d}H_0(\cdot)}|D) &\propto \textnormal{Poisson}\left( Y_i(t)e^{\left( \beta x_i(t) \right)} \mathop{\textnormal{d}H_0(t)} \right) \\ \pause
\mathop{\textnormal{d}H_0(\cdot)} &\sim \textnormal{Gamma}\left( c \mathop{\textnormal{d}H_0^*(t)} , c\right) \\ \pause
\beta &\sim \textnormal{N}\left( \mu, \tau \right)
\end{aligned}
\end{equation} \pause

\begin{equation}
\nonumber
\label{posterior}
\begin{aligned}
p(\beta, \mathop{\textnormal{d}H_0(\cdot)} | D) &\propto L(\beta,\mathop{\textnormal{d}H_0(\cdot)}|D) \times p(\beta) \times p(\mathop{\textnormal{d}H_0(\cdot)}) \\
\textnormal{Posterior} &\propto \textnormal{Likelihood} \times \textnormal{Priors}
\end{aligned}
\end{equation}

\end{frame}

%====================================================================
% Section 4
%====================================================================
\section{Examples}

%====================================================================
% Frame 4.1
%====================================================================
\begin{frame}[fragile]
  \frametitle{Install bsm}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{devtools}\hlopt{::}\hlkwd{install_bitbucket}\hlstd{(}\hlstr{"bklamer/bsm"}\hlstd{)}
\end{alltt}
\end{kframe}
\end{knitrout}
\end{frame}

%====================================================================
% Frame 4.2
%====================================================================
\begin{frame}[fragile]
  \frametitle{Function Arguments}
\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{bsm}\hlstd{(}
  \hlstd{formula,}
  \hlkwc{data} \hlstd{=} \hlkwa{NULL}\hlstd{,}
  \hlkwc{id} \hlstd{=} \hlkwa{NULL}\hlstd{,}
  \hlkwc{model_selection} \hlstd{=} \hlkwa{NULL}\hlstd{,}
  \hlkwc{n_chains} \hlstd{=} \hlnum{2}\hlstd{,}
  \hlkwc{n_cores} \hlstd{=} \hlnum{1}\hlstd{,}
  \hlkwc{warmup} \hlstd{=} \hlnum{2000}\hlstd{,}
  \hlkwc{adapt} \hlstd{=} \hlnum{1000}\hlstd{,}
  \hlkwc{iter} \hlstd{=} \hlnum{4000}\hlstd{,}
  \hlkwc{thin} \hlstd{=} \hlnum{1}\hlstd{,}
  \hlkwc{method} \hlstd{=} \hlstr{"jags"}\hlstd{,}
  \hlkwc{inits} \hlstd{=} \hlkwa{NULL}\hlstd{,}
  \hlkwc{c} \hlstd{=} \hlnum{0.001}\hlstd{,}
  \hlkwc{r} \hlstd{=} \hlnum{0.1}\hlstd{,}
  \hlkwc{mu} \hlstd{=} \hlnum{0}\hlstd{,}
  \hlkwc{tau} \hlstd{=} \hlnum{0.0001}
\hlstd{)}
\end{alltt}
\end{kframe}
\end{knitrout}
\end{frame}

%====================================================================
% Frame 4.3
%====================================================================
\begin{frame}[fragile]
  \frametitle{Leukemia}
% latex table generated in R 3.2.2 by xtable 1.8-0 package
% Sun Nov  8 14:14:52 2015
\begin{table}[ht]
\centering
\caption{The Gehan-Frierich leukemia dataset; $N=42$} 
\label{table:leukemia}
\begin{tabular}{rrlr}
  \hline
time & event & treatment & pair \\ 
  \hline
  6 &   1 & 6-MP &   6 \\ 
    6 &   1 & 6-MP &  14 \\ 
    6 &   1 & 6-MP &  17 \\ 
    6 &   0 & 6-MP &  20 \\ 
    7 &   1 & 6-MP &   2 \\ 
    9 &   0 & 6-MP &  19 \\ 
   \hline
\end{tabular}
\end{table}

\end{frame}

%====================================================================
% Frame 4.4
%====================================================================
\begin{frame}[fragile]
  \frametitle{Leukemia}

\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{survival} \hlkwb{<-} \hlkwd{coxph}\hlstd{(}\hlkwd{Surv}\hlstd{(time, event)} \hlopt{~} \hlstd{treatment,} \hlkwc{data} \hlstd{= leukemia)}
\hlstd{bsm} \hlkwb{<-} \hlkwd{bsm}\hlstd{(}\hlkwd{Surv}\hlstd{(time, event)} \hlopt{~} \hlstd{treatment,} \hlkwc{data} \hlstd{= leukemia)}
\hlstd{bsm}
\end{alltt}
\end{kframe}
\end{knitrout}

\begin{knitrout}\scriptsize 
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{verbatim}
## 
## JAGS model summary statistics from 8000 samples (chains = 2; adapt+burnin = 3000):
##                                                                          
##                       Lower95 Median Upper95   Mean      SD Mode    MCerr
## beta_treatmentcontrol 0.72959 1.5342  2.3703 1.5485 0.42095   -- 0.013815
##                                                   
##                       MC%ofSD SSeff   AC.10   psrf
## beta_treatmentcontrol     3.3   928 0.10497 1.0001
## 
## Total time taken: 3.9 seconds
\end{verbatim}
\end{kframe}
\end{knitrout}

$e^{\beta_{treatment}} = e^{1.53} = 4.64$

\end{frame}

%====================================================================
% Frame 4.5
%====================================================================
\begin{frame}[fragile]
  \frametitle{Leukemia}

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\maxwidth]{figure/fit-leukemia-density-1} 

}



\end{knitrout}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\maxwidth]{figure/fit-leukemia-trace-1} 

}



\end{knitrout}

\end{frame}

%====================================================================
% Frame 4.6
%====================================================================
\begin{frame}[fragile]
  \frametitle{Leukemia JAGS Model}

\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
model \{
  \hlkwd{for}(j in 1:n_unique_events) \{
    \hlkwd{for}(i in 1:n_subjects) \{
      counting_proc[i,j] ~ \hlkwd{dpois}(intensity_proc[i,j]) \hlcom{# Likelihood}
      intensity_proc[i,j] <- is_at_risk[i,j] *
        \hlkwd{exp}(beta_treatmentcontrol * treatmentcontrol[i]) *
        hazard_proc[j] \hlcom{# Intensity}
    \}
    hazard_proc[j] ~ \hlkwd{dgamma}(mu[j], c)
    mu[j] <- hazard_proc_mean[j] * c \hlcom{# prior mean hazard}
    hazard_proc_mean[j] <- r * (time_unique_events_plus[j+1] - 
                           time_unique_events_plus[j])
  \}
  beta_treatmentcontrol ~ \hlkwd{dnorm}(beta_mu, beta_tau) 
\}
\end{alltt}
\end{kframe}
\end{knitrout}

\end{frame}

%====================================================================
% Frame 4.7
%====================================================================
\begin{frame}[fragile]
  \frametitle{heart}
% latex table generated in R 3.2.2 by xtable 1.8-0 package
% Sun Nov  8 14:17:09 2015
\begin{table}[ht]
\centering
\caption{The Stanford heart transplant dataset; $N=103$} 
\label{table:heart}
\begin{tabular}{rrrrrrrr}
  \hline
id & start & stop & event & age & year & surgery & transplant \\ 
  \hline
  1 &   0 &  50 &   1 & -17.16 & 0.12 &   0 &   0 \\ 
    2 &   0 &   6 &   1 & 3.84 & 0.25 &   0 &   0 \\ 
    3 &   0 &   1 &   0 & 6.30 & 0.27 &   0 &   0 \\ 
    3 &   1 &  16 &   1 & 6.30 & 0.27 &   0 &   1 \\ 
    4 &   0 &  36 &   0 & -7.74 & 0.49 &   0 &   0 \\ 
    4 &  36 &  39 &   1 & -7.74 & 0.49 &   0 &   1 \\ 
   \hline
\end{tabular}
\end{table}

\end{frame}

%====================================================================
% Frame 4.8
%====================================================================
\begin{frame}[fragile]
  \frametitle{Heart}
\begin{knitrout}\scriptsize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{survival} \hlkwb{<-} \hlkwd{coxph}\hlstd{(}\hlkwd{Surv}\hlstd{(start, stop, event)} \hlopt{~} \hlstd{transplant,} \hlkwc{data} \hlstd{= heart)}
\hlstd{bsm} \hlkwb{<-} \hlkwd{bsm}\hlstd{(}\hlkwd{Surv}\hlstd{(start, stop, event)} \hlopt{~} \hlstd{transplant,} \hlkwc{id} \hlstd{=} \hlstr{"id"}\hlstd{,} \hlkwc{data} \hlstd{= heart)}
\hlstd{bsm}
\end{alltt}
\end{kframe}
\end{knitrout}

\begin{knitrout}\scriptsize 
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{verbatim}
## 
## JAGS model summary statistics from 8000 samples (chains = 2; adapt+burnin = 3000):
##                                                                      
##                  Lower95  Median Upper95   Mean      SD Mode    MCerr
## beta_transplant -0.44112 0.11424 0.75193 0.1183 0.30465   -- 0.011135
##                                             
##                 MC%ofSD SSeff   AC.10   psrf
## beta_transplant     3.7   749 0.13774 1.0025
## 
## Total time taken: 45.4 seconds
\end{verbatim}
\end{kframe}
\end{knitrout}

\end{frame}

%====================================================================
% Frame 4.9
%====================================================================
\begin{frame}[fragile]
  \frametitle{Heart}

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\maxwidth]{figure/fit-heart-density-1} 

}



\end{knitrout}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\maxwidth]{figure/fit-heart-trace-1} 

}



\end{knitrout}

\end{frame}

%====================================================================
% Frame 4.10
%====================================================================
\begin{frame}[fragile]
  \frametitle{Sage-Grouse}

\begin{figure}
\caption{Male sage-grouse {\scriptsize by Bob Wick, BLM, CC-BY 2.0 license.}}
\includegraphics[scale=1]{ext-fig/male-sage-grouse.jpg} 
\end{figure}

\end{frame}

%====================================================================
% Frame 4.11
%====================================================================
\begin{frame}[fragile]
  \frametitle{Sage-Grouse}

\begin{figure}
\caption{Female sage-grouse {\scriptsize by Nick Myatt, ODFW, CC-BY-SA 2.0 license.}}
\includegraphics[scale=1]{ext-fig/female-sage-grouse.jpg} 
\end{figure}

\end{frame}

%====================================================================
% Frame 4.12
%====================================================================
\begin{frame}[fragile]
  \frametitle{Sage-Grouse}

% latex table generated in R 3.2.2 by xtable 1.8-0 package
% Wed Nov 11 12:32:46 2015
\begin{table}[ht]
\centering
\caption{The female Sage-Grouse dataset by Chris Kirol and Jeff Beck \cite{kirol2015}; $N=129$} 
\label{table:sage-grouse}
{\footnotesize
\begin{tabular}{lrrrrrr}
  \hline
id & start & stop & event & roughness & edge\_distance & shrub\_height \\ 
  \hline
1a &   0 &  18 &   0 & 0.27 & 0.43 & 8.58 \\ 
  1a &  18 &  37 &   0 & 0.28 & 0.46 & 8.04 \\ 
  1a &  37 &  39 &   0 & 0.25 & 0.35 & 7.92 \\ 
  1a &  39 &  45 &   0 & 0.26 & 0.38 & 7.82 \\ 
  1a &  45 &  71 &   0 & 0.26 & 0.37 & 7.80 \\ 
  1a &  71 &  74 &   0 & 0.34 & 0.35 & 7.85 \\ 
   \hline
\end{tabular}
}
\end{table}


\end{frame}

%====================================================================
% Frame 4.13
%====================================================================
\begin{frame}[fragile]
  \frametitle{Sage-Grouse}

\begin{knitrout}\scriptsize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlcom{# Remove NA cases}
\hlstd{missing} \hlkwb{<-} \hlstd{female[}\hlkwd{is.na}\hlstd{(female}\hlopt{$}\hlstd{shrub_height), ][[}\hlnum{1}\hlstd{]]}
\hlstd{female} \hlkwb{<-} \hlstd{female} \hlopt{%>%} \hlkwd{filter}\hlstd{(}\hlopt{!}\hlstd{(id} \hlopt{%in%} \hlstd{missing))} \hlopt{%>%} \hlkwd{droplevels}\hlstd{(.)}
\hlcom{# Female data model}
\hlstd{bsm} \hlkwb{<-} \hlkwd{bsm}\hlstd{(}\hlkwd{Surv}\hlstd{(start, stop, event)} \hlopt{~} \hlstd{roughness} \hlopt{+} \hlstd{edge_distance} \hlopt{+}
                 \hlstd{shrub_height,} \hlkwc{id} \hlstd{=} \hlstr{"id"}\hlstd{,} \hlkwc{data} \hlstd{= female)}
\end{alltt}
\end{kframe}
\end{knitrout}

\begin{knitrout}\scriptsize 
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{verbatim}
## 
## JAGS model summary statistics from 8000 samples (chains = 2; adapt+burnin = 3000):
##                                                                    
##                     Lower95   Median  Upper95     Mean      SD Mode
## beta_roughness     -0.64351  0.27506   1.2704   0.2753 0.47893   --
## beta_edge_distance  0.25105  0.68114   1.1341   0.6751 0.22615   --
## beta_shrub_height  -0.47694 -0.18049 0.097198 -0.18084  0.1438   --
##                                                          
##                        MCerr MC%ofSD SSeff   AC.10   psrf
## beta_roughness       0.01586     3.3   912 0.12486 1.0006
## beta_edge_distance 0.0075654     3.3   894 0.11172 1.0003
## beta_shrub_height   0.010591     7.4   184 0.63361 1.0052
## 
## Total time taken: 2.4 minutes
\end{verbatim}
\end{kframe}
\end{knitrout}

$e^{\beta_{transplant}} = e^{0.11} = 1.12$

\end{frame}

%====================================================================
% Frame 4.14
%====================================================================
\begin{frame}[fragile]
  \frametitle{Sage-Grouse}

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\maxwidth]{figure/fit-female-density-1} 

}



\end{knitrout}

\end{frame}

%====================================================================
% Frame 4.15
%====================================================================
\begin{frame}[fragile]
  \frametitle{Sage-Grouse}

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\maxwidth]{figure/fit-female-trace-1} 

}



\end{knitrout}

\end{frame}

%====================================================================
% Section 5
%====================================================================
\section{Conclusion}

%====================================================================
% Frame 5.1
%====================================================================
\begin{frame}[fragile]
  \frametitle{bsm}
  \textbf{bsm} is being developed to 
  \begin{itemize}
  \item be user friendly
  \item use a \textbf{survival}-like syntax
  \item provide a variety of models
  \item provide a variety of survival plots
  \item use modern Bayesian tools
  \end{itemize} \pause
  Current status: proof of concept
\end{frame}

%====================================================================
% Frame 5.2
%====================================================================
\begin{frame}[fragile]
  \frametitle{Future Versions}
  \begin{itemize}
  \item Survival plots
  \item Parametric models
  \item Semi-parametric models with smoothed hazards
  \item Joint models
  \item Improved everything
  \end{itemize}
\end{frame}

%====================================================================
% Frame 6.0
%====================================================================
\plain{Questions?}

%====================================================================
% Frame 7.0
%====================================================================
\begin{frame}[allowframebreaks]

  \frametitle{References}

  \bibliography{bibliography}
  \bibliographystyle{abbrv}

\end{frame}

\end{document}
