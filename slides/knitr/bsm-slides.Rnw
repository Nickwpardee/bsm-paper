\documentclass[11pt]{beamer}

\usetheme{m}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}
\usepackage{subcaption}  % an alternative package for sub figures
\newcommand{\subfloat}[2][need a sub-caption]{\subcaptionbox{#1}{#2}}
\usepackage{graphicx}
\usepackage[final]{microtype}

\title{{\normalfont \bfseries bsm}}
\subtitle{An R Package for Bayesian Survival Modeling}
\date{\today}
\author{Brett Klamer}
\institute{University of Wyoming}
\titlegraphic{\hfill\includegraphics[height=1.5cm]{ext-fig/logo.png}}

\begin{document}

<<setup, include=FALSE, cache=FALSE>>=
library(knitr)
library(ggplot2)
library(bsm)
library(ggmcmc)
library(tikzDevice)
library(xtable)
library(dplyr)

# inline hook to process the output of \Sexpr{} statements to just 2 digits
inline_hook <- function(x) {
  if(is.numeric(x)) x <- round(x, 2)
  paste(as.character(x), collapse=", ")
}
knit_hooks$set(inline = inline_hook)

# cache all chunks
opts_chunk$set(cache = TRUE)

# no dingbats in pdf graphs
pdf.options(useDingbats = FALSE) # unnecessary with tikz, but a good idea...

#==============================================================================
## Embed latex font in R graphics
# Use tikz, but have Cairo as a backup.
# when using tikz, make sure pgf and preview packages are installed in latex.
# DO NOT load those packages in the preamble
#==============================================================================
#options(tikzMetricsDictionary = ’/path/to/dictionary/location’)
#library(tikzDevice)
#opts_chunk$set(dev = 'tikz')
#opts_chunk$set(dev.args=list(pointsize=10))

library(Cairo)
myfont <- "Garamond"
CairoFonts(regular = paste(myfont, "style = Regular", sep = ":"),
           bold = paste(myfont, "style = Bold", sep = ":"),
           italic = paste(myfont, "style = Italic", sep = ":"),
           bolditalic = paste(myfont, "style = Bold Italic, BoldItalic", sep = ":"))
pdf <- CairoPDF
opts_chunk$set(dev = 'CairoPDF')
opts_chunk$set(dev.args=list(pointsize=10))
@

\maketitle

\begin{frame}
  \frametitle{Table of Contents}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents[hideallsubsections]
\end{frame}

%====================================================================
% Section 1
%====================================================================
\section{Survival Analysis}

%====================================================================
% Frame 1.1
%====================================================================
\begin{frame}[fragile]
  \frametitle{What is survival analysis?}
  Models relationship between covariates and event using time. \pause

  \begin{itemize}
    \item Time
  \begin{itemize}
  \item The number of (hours, days, months, years) between two time points. \pause
  \item Each subject is assumed to start at the time $t=0$ point. \pause
  \end{itemize}
  \item Event
  \begin{itemize}
  \item Any change in a subject's status. \pause
  \item Death, disease, equipment failure, birth, remission, etc. \pause
  \end{itemize}
  \item Model
  \begin{itemize}
  \item $Y \sim X$
  \end{itemize}
  \end{itemize}
\end{frame}

%====================================================================
% Frame 1.2
%====================================================================
\begin{frame}[fragile]
  \frametitle{What is special about survival analysis?}
  \onslide<1-> When the event time is unknown.
  \begin{itemize}
  \onslide<2-> \item Right-censoring
  \begin{itemize}
  \onslide<3-> \item Subject never experiences event during study time.
  \end{itemize}
  \onslide<2-> \item Left-censoring
  \begin{itemize}
  \onslide<4-> \item Subject experienced event before first measurement.
  \end{itemize}
  \onslide<2-> \item Interval-censoring
  \begin{itemize}
  \onslide<5-> \item Subject experiences event between two known time points.
  \onslide<6-> \item Occurs with periodic follow ups.
  \end{itemize}
  \end{itemize}
\end{frame}

%====================================================================
% Frame 1.3
%====================================================================
\begin{frame}[fragile]
  \frametitle{What is special about survival analysis?}
<<right-censor-graphic, dev = 'tikz', echo = FALSE, fig.cap = 'Example of Right Censoring', fig.lp = 'fig:', fig.width = 5, fig.height = 2.5, fig.align = 'center', fig.pos = 'ht'>>=
xstart <- c(0, 0, 4, 1, 0)
xend <- c(10, 8, 7, 5, 9)
ystart <- c(5, 4, 3, 2, 1)
yend <- c(5, 4, 3, 2, 1)
type <- c("censor", "event", "event", "censor", "event")
subject <- c("Subject 1", "Subject 2", "Subject 3", "Subject 4", "Subject 5")
data <- data.frame(xstart, xend, ystart, yend, type, subject)
ggplot(data, aes(y = subject)) + 
  labs(x = "Survival Time in Weeks", y = "Subject") + 
  geom_point(aes(x = data$"xstart"), size = 2.75, shape = 1) +
  geom_point(aes(x = data$"xend", shape = type), size = 2.75) +
  scale_shape_manual(values = c(4, 19), 
                     name = "Type", 
                     labels = c("Right Censor", "Event Occurs")) +
  geom_segment(aes(x = xstart, y = subject, xend = xend, yend = subject)) + 
  theme_bw() + 
  theme(axis.title.y = element_blank())
@
\end{frame}

%====================================================================
% Frame 1.4
%====================================================================
\begin{frame}[fragile]
  \frametitle{What is special about survival analysis?}
  \onslide<1-> When covariates change over time.
  \begin{itemize}
  \onslide<2-> \item Internal time-dependent covariate
  \begin{itemize}
  \onslide<3-> \item Variability in measurement is based on characteristics or behavior of subject.
  \onslide<4-> \item Blood pressure, disease complications, etc.
  \end{itemize}
  \onslide<2-> \item External time-dependent covariate
  \begin{itemize}
  \onslide<5-> \item Variability in measurement is based on outside forces and may affect all subjects.
  \onslide<6-> \item Environmental conditions, age, etc.
  \end{itemize}
  \end{itemize}
\end{frame}

%====================================================================
% Frame 1.5
%====================================================================
\begin{frame}[fragile]
  \frametitle{Basic methods}
  Survival function: $S(t) = P(T > t)$
  
  <<theoretical_survival_curve, dev = 'tikz', echo = FALSE, fig.cap = 'Theoretical graph of \\(S(t)\\)', fig.lp = 'fig:', fig.align = 'center', fig.width = 4, fig.height = 2.5>>=
library(ggplot2)
x <- c(-4:4)
dat <- data.frame(x, y = -1/(1+exp(-x)))
dat2 <- data.frame(x = c(-3.5, 3.45), y = c(-0.08, -0.91))
f <- function(x) -1/(1+exp(-x))
ggplot(aes(x,y), data = dat) + 
  stat_function(fun=f, colour="red") +
  labs(x = "$t$", y = "$S(t)$") + 
  scale_y_continuous(breaks=c(-1, 0), labels=c(0, 1)) +
  scale_x_continuous(breaks=c(-4, 4), labels=c(0, "$\\infty$")) +
  geom_text(aes(x, y, label = c("$S(0) = 1$", "$S(\\infty) = 0$")), 
            data = dat2, size = 2.5)+
  theme_bw()
@  
  
\end{frame}

%====================================================================
% Frame 1.6
%====================================================================
\begin{frame}[fragile]
  \frametitle{Basic Methods}
  Hazard Function
  \begin{equation}
  \nonumber
  \begin{aligned}
  h(t) &= -\frac{d}{dt} \log S(t) \\
  H(t) &= - \log S(t)
  \end{aligned}
  \end{equation}
<<theoretical-hazard-graphs, dev = 'tikz', echo = FALSE, fig.cap='Types of Hazard Curves', fig.subcap=c('Theoretical Hazard Curves', 'Theoretical Cumulative Hazard Curve'), fig.width = 2.9, fig.height = 1.875, out.width='.49\\linewidth'>>=
data <- data.frame(x = -4:4)
f_inc <- function(x) 1.5^x * 50.5
f_dec <- function(x) 1.5^-x * 50.5
f_bath <- function(x) x^4
ggplot(data, aes(x = x)) + 
  stat_function(fun = f_inc, aes(colour = "Increasing Hazard")) +
  stat_function(fun = f_dec, aes(colour = "Decreasing Hazard")) + 
  stat_function(fun = f_bath, aes(colour = "Bathtub Hazard")) + 
  scale_colour_manual(values = c("red", "blue", "green")) +
  theme_bw() +
  scale_x_continuous(breaks=-4, labels = "0") +
  scale_y_continuous(breaks=0, labels = "0") +
  labs(x = "$t$", y = "$h(t)$", fill = NULL) +
  theme(legend.position = c(5, 5), # to see it, set at c(0.55, 0.75)
        legend.text=element_text(size=8),
        legend.title=element_blank(),
        legend.background = element_blank())

ggplot(data, aes(x = x)) + 
  stat_function(fun = f_inc) +
  scale_x_continuous(breaks=-4, labels = "0") +
  scale_y_continuous(breaks=10, labels = "0") +
  labs(x = "$t$", y = "$H(t)$", fill = NULL) + 
  theme_bw()
@

\end{frame}

%====================================================================
% Frame 1.7
%====================================================================
\begin{frame}[fragile]
  \frametitle{Regression Model}
  Cox Proportional Hazards Regression Model \cite{cox1972}
  \begin{equation}
  \nonumber
  \begin{aligned}
  h_i(t;\mathbf{X}) &= h_0(t)e^{\sum_{j=1}^p(\beta_{j}X_{ij})}
  \end{aligned}
  \end{equation}
  \pause
  \begin{itemize}
  \item Assumes proportional hazards \pause
  \item For right-censored data \pause
  \item Leaves $h_0(t)$ unspecified through use of partial likelihood \pause
  \item Inference is focused on $e^{\beta} = \textnormal{Hazard Ratio}$ \pause
    \item Extensions accommodate time-dependent covariates
  \end{itemize}
\end{frame}

%====================================================================
% Section 2
%====================================================================
\section{Overview of Survival Packages in R}

%====================================================================
% Frame 2.1
%====================================================================
\begin{frame}[fragile]
  \frametitle{What is R?}
  R is an interpreted, dynamic, functional language with object oriented data structures and methods. \cite{morandat2012, wickham2011} \pause
  \begin{itemize}
  \item Package - Collection of documented functions and data that can be shared with others. \pause
  \item Library - Directory of packages.
  \end{itemize}
\end{frame}

%====================================================================
% Frame 2.2
%====================================================================
\begin{frame}[fragile]
  \frametitle{``Survival'' package in R}
  \textbf{survival} package by Therneau \cite{survival} \pause
  \begin{itemize}
  \item Classical inference \pause
  \item The defining survival analysis package \pause
  \item Provides methods for beginning to end of an analysis \pause
  \item What \texttt{lm()} is to regression, \textbf{survival} is to survival analysis
  \end{itemize}
\end{frame}

%====================================================================
% Frame 2.3
%====================================================================
\begin{frame}[fragile]
  \frametitle{Other Classical Inference Packages}
coxphf, coxphw, coxrobust, CPHshape, eha, Icsurv, intcox, mfp, MIICD, multcomp, multipleNCC, NestedCohort, OrdFacReg, paf, rms, smoothHR, survey, survivalMPL, timereg, etc.

\end{frame}

%====================================================================
% Frame 2.4
%====================================================================
\begin{frame}[fragile]
  \frametitle{Bayesian packages}
  No Bayesian equivalent of the \textbf{survival} package
  \begin{itemize} \pause
  \item Enter data how? \pause
  \item Survival model? \pause
  \item Model selection? \pause
  \item Sampling? \pause
  \end{itemize}
  BaSTA, bayesSurv, BayHaz, BGPhazard, BMA, bsm, DPpackage, dynsurv, IDPSurvival, LearnBayes, mixAK, MRH, psbcGroup, SemiCompRisks, spatsurv, spBayesSurv, MCMCglmm, MCMCpack, PReMiuM, etc.
\end{frame}

%====================================================================
% Frame 2.5
%====================================================================
\begin{frame}[fragile]
  \frametitle{DPpackage vs. bsm}
  \textbf{DPpackage} offers modeling with time-dependent covariates, random effects, model selection, and interval censoring. \cite{dppackage} \pause
  \begin{itemize}
  \item Doesn't use a \textbf{survival}-like syntax \pause
  \item Complicated models \pause
  \begin{itemize}
  \item Generalized linear mixed models with Dirichlet process mixtures
  \end{itemize} \pause
  \item 268 page manual with 59 functions
  \end{itemize}
\end{frame}

%====================================================================
% Section 3
%====================================================================
\section{bsm}

%====================================================================
% Frame 3.1
%====================================================================
\begin{frame}[fragile]
  \frametitle{Package Framework}

\begin{itemize}
\item \textit{Writing R Extensions} by the R Core Team \cite{r-ext}
\item \textit{R Packages} by Wickham \cite{wickham2015}
\item devtools package by Wickham and Chang \cite{devtools}
\item roxygen2 package by Wickham, et al. \cite{roxygen2}
\item testthat package by Wickham \cite{testthat}
\item rmarkdown package by Xie, et al. \cite{rmarkdown}
\end{itemize}

\end{frame}

%====================================================================
% Frame 3.2
%====================================================================
\begin{frame}[fragile]
  \frametitle{bsm}

\begin{figure}
\includegraphics[scale=1]{ext-fig/bsm-folder.pdf} 
\end{figure}

\end{frame}

%====================================================================
% Frame 3.3
%====================================================================
\begin{frame}[fragile]
  \frametitle{bsm}
  \textbf{bsm} offers survival modeling with time-dependent covariates, random effects, model selection, and interval censoring. \pause
  \begin{itemize}
  \item \textbf{survival}-like syntax \pause
  \item Uses common Gamma process Cox model \pause
  \item Default weakly informative priors \pause
  \item Sampling through JAGS or Stan
  \end{itemize}
\end{frame}

%====================================================================
% Frame 3.4
%====================================================================
\begin{frame}[fragile]
  \frametitle{Counting process model}
  Cox-type counting process model \cite{aalen1975}
  \begin{equation}
  \nonumber
  \begin{aligned}
  \lambda_i(t;\mathbf{X}(t)) &= Y_i(t) h_0(t)e^{\sum_{j=1}^p(\beta_{j}X_{ij}(t))} \\ 
  D &= \{ K_i(t), Y_i(t), x_i(t); i = 1,\ldots, n\}
  \end{aligned}
  \end{equation}
  \pause
  \begin{itemize}
  \item Right-censored data \pause
  \item Time-dependent covariates \pause
  \item Using Bayesian framework so specify $H_0(t)$ as a step process \pause
  \item Possible to accommodate time-dependent strata, left truncation, multiple events per subject
  \end{itemize}
\end{frame}

%====================================================================
% Frame 3.5
%====================================================================
\begin{frame}[fragile]
  \frametitle{Bayesian Inference}
  \begin{equation}
  \nonumber
\label{propto-likelihood}
\begin{aligned}
L_i(\beta,\mathop{\textnormal{d}H_0(\cdot)}|D) &\propto \textnormal{Poisson}\left( Y_i(t)e^{\left( \beta x_i(t) \right)} \mathop{\textnormal{d}H_0(t)} \right) \\ \pause
\mathop{\textnormal{d}H_0(\cdot)} &\sim \textnormal{Gamma}\left( c \mathop{\textnormal{d}H_0^*(t)} , c\right) \\ \pause
\beta &\sim \textnormal{N}\left( \mu, \tau \right)
\end{aligned}
\end{equation} \pause

\begin{equation}
\nonumber
\label{posterior}
\begin{aligned}
p(\beta, \mathop{\textnormal{d}H_0(\cdot)} | D) &\propto L(\beta,\mathop{\textnormal{d}H_0(\cdot)}|D) \times p(\beta) \times p(\mathop{\textnormal{d}H_0(\cdot)}) \\
\textnormal{Posterior} &\propto \textnormal{Likelihood} \times \textnormal{Priors}
\end{aligned}
\end{equation}

\end{frame}

%====================================================================
% Section 4
%====================================================================
\section{Examples}

%====================================================================
% Frame 4.1
%====================================================================
\begin{frame}[fragile]
  \frametitle{Install bsm}
  <<install-bsm, eval = FALSE>>=
  devtools::install_bitbucket("bklamer/bsm")
  @
\end{frame}

%====================================================================
% Frame 4.2
%====================================================================
\begin{frame}[fragile]
  \frametitle{Function Arguments}
<<args, eval = FALSE, size = "footnotesize">>=
bsm(
  formula,
  data = NULL,
  id = NULL,
  model_selection = NULL,
  n_chains = 2,
  n_cores = 1,
  warmup = 2000,
  adapt = 1000,
  iter = 4000,
  thin = 1,
  method = "jags",
  inits = NULL,
  c = 0.001,
  r = 0.1,
  mu = 0,
  tau = 0.0001
)
@
\end{frame}

%====================================================================
% Frame 4.3
%====================================================================
\begin{frame}[fragile]
  \frametitle{Leukemia}
<<leukemia-data, results = "asis", echo = FALSE>>=
df <- leukemia
print(xtable(head(df), caption='The Gehan-Frierich leukemia dataset; $N=42$', label='table:leukemia'), caption.placement = "top", include.rownames=FALSE)
@
\end{frame}

%====================================================================
% Frame 4.4
%====================================================================
\begin{frame}[fragile]
  \frametitle{Leukemia}

<<fit-leukemia-no, eval = FALSE, tidy = TRUE, size = 'footnotesize'>>=
survival <- coxph(Surv(time, event) ~ treatment, data = leukemia)
bsm <- bsm(Surv(time, event) ~ treatment, data = leukemia)
bsm
@
<<fit-leukemia-yes, echo = FALSE, message = FALSE, results = 'hide'>>=
leukemia_model <- bsm(Surv(time, event) ~ treatment, data = leukemia)
@
<<fit-leukemia-print, echo = FALSE, message = FALSE, size = 'scriptsize '>>=
leukemia_model
leukemia_median <- leukemia_model$summary$quantiles[3]
leukemia_hr <- exp(leukemia_median)
@

$e^{\beta_{treatment}} = e^{\Sexpr{leukemia_median}} = \Sexpr{leukemia_hr}$

\end{frame}

%====================================================================
% Frame 4.5
%====================================================================
\begin{frame}[fragile]
  \frametitle{Leukemia}

<<fit-leukemia-density, echo = FALSE, fig.lp = 'fig:', fig.width = 4, fig.height = 1.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_density(ggs(leukemia_model$mcmc))
@
<<fit-leukemia-trace, echo = FALSE, fig.lp = 'fig:', fig.width = 4, fig.height = 1.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_traceplot(ggs(leukemia_model$mcmc))
@

\end{frame}

%====================================================================
% Frame 4.6
%====================================================================
\begin{frame}[fragile]
  \frametitle{Leukemia JAGS Model}

<<leukemia-jags-model, eval = FALSE, tidy = TRUE, size = "footnotesize">>=
model {
  for(j in 1:n_unique_events) {
    for(i in 1:n_subjects) {
      counting_proc[i,j] ~ dpois(intensity_proc[i,j]) # Likelihood
      intensity_proc[i,j] <- is_at_risk[i,j] *
        exp(beta_treatmentcontrol * treatmentcontrol[i]) *
        hazard_proc[j] # Intensity
    }
    hazard_proc[j] ~ dgamma(mu[j], c)
    mu[j] <- hazard_proc_mean[j] * c # prior mean hazard
    hazard_proc_mean[j] <- r * (time_unique_events_plus[j+1] - 
                           time_unique_events_plus[j])
  }
  beta_treatmentcontrol ~ dnorm(beta_mu, beta_tau) 
}
@

\end{frame}

%====================================================================
% Frame 4.7
%====================================================================
\begin{frame}[fragile]
  \frametitle{heart}
<<heart-data, results = "asis", echo = FALSE>>=
df <- heart
print(xtable(head(df), caption='The Stanford heart transplant dataset; $N=103$', label='table:heart'), caption.placement = "top", include.rownames=FALSE)
@
\end{frame}

%====================================================================
% Frame 4.8
%====================================================================
\begin{frame}[fragile]
  \frametitle{Heart}
<<fit-heart-no, eval = FALSE, tidy = TRUE, size = 'scriptsize'>>=
survival <- coxph(Surv(start, stop, event) ~ transplant, data = heart)
bsm <- bsm(Surv(start, stop, event) ~ transplant, id = "id", data = heart)
bsm
@
<<fit-heart-yes, echo = FALSE, message = FALSE, results = 'hide'>>=
heart_model <- bsm(Surv(start, stop, event) ~ transplant, id = "id", data = heart)
@
<<fit-heart-print, echo = FALSE, message = FALSE, size = 'scriptsize '>>=
heart_model
heart_median <- heart_model$summary$quantiles[3]
heart_hr <- exp(heart_median)
@

\end{frame}

%====================================================================
% Frame 4.9
%====================================================================
\begin{frame}[fragile]
  \frametitle{Heart}

<<fit-heart-density, echo = FALSE, fig.lp = 'fig:', fig.width = 4, fig.height = 1.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_density(ggs(heart_model$mcmc))
@
<<fit-heart-trace, echo = FALSE, fig.lp = 'fig:', fig.width = 4, fig.height = 1.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_traceplot(ggs(heart_model$mcmc))
@

\end{frame}

%====================================================================
% Frame 4.10
%====================================================================
\begin{frame}[fragile]
  \frametitle{Sage-Grouse}

\begin{figure}
\caption{Male sage-grouse {\scriptsize by Bob Wick, BLM, CC-BY 2.0 license.}}
\includegraphics[scale=1]{ext-fig/male-sage-grouse.jpg} 
\end{figure}

\end{frame}

%====================================================================
% Frame 4.11
%====================================================================
\begin{frame}[fragile]
  \frametitle{Sage-Grouse}

\begin{figure}
\caption{Female sage-grouse {\scriptsize by Nick Myatt, ODFW, CC-BY-SA 2.0 license.}}
\includegraphics[scale=1]{ext-fig/female-sage-grouse.jpg} 
\end{figure}

\end{frame}

%====================================================================
% Frame 4.12
%====================================================================
\begin{frame}[fragile]
  \frametitle{Sage-Grouse}

<<female-data, results = "asis", echo = FALSE>>=
df <- female
print(xtable(head(df), caption='The female Sage-Grouse dataset by Chris Kirol and Jeff Beck \\cite{kirol2015}; $N=129$', label='table:sage-grouse'), caption.placement = "top", include.rownames=FALSE, size = "footnotesize")
@

\end{frame}

%====================================================================
% Frame 4.13
%====================================================================
\begin{frame}[fragile]
  \frametitle{Sage-Grouse}

<<fit-female-no, eval = FALSE, tidy = FALSE, size = 'scriptsize'>>=
# Remove NA cases
missing <- female[is.na(female$shrub_height), ][[1]]
female <- female %>% filter(!(id %in% missing)) %>% droplevels(.)
# Female data model
bsm <- bsm(Surv(start, stop, event) ~ roughness + edge_distance + 
                 shrub_height, id = "id", data = female)
@
<<fit-female-yes, echo = FALSE, message = FALSE, results = 'hide'>>=
missing <- female[is.na(female$shrub_height), ][[1]]
female <- female %>% filter(!(id %in% missing)) %>% droplevels(.)
# Female data model
female_model <- bsm(Surv(start, stop, event) ~ roughness + edge_distance + shrub_height, id = "id", data = female)
@
<<fit-female-print, echo = FALSE, message = FALSE, size = 'scriptsize '>>=
female_model
female_median_roughness <- female_model$summary$quantiles[1,3]
female_median_edge <- female_model$summary$quantiles[2,3]
female_median_shrub <- female_model$summary$quantiles[3,3]
female_hazard_roughness <- exp(female_median_roughness)
female_hazard_edge <- exp(female_median_edge)
female_hazard_shrub <- exp(female_median_shrub)
@

$e^{\beta_{transplant}} = e^{\Sexpr{heart_median}} = \Sexpr{heart_hr}$

\end{frame}

%====================================================================
% Frame 4.14
%====================================================================
\begin{frame}[fragile]
  \frametitle{Sage-Grouse}

<<fit-female-density, echo = FALSE, fig.lp = 'fig:', fig.width = 5, fig.height = 3.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_density(ggs(female_model$mcmc))
@

\end{frame}

%====================================================================
% Frame 4.15
%====================================================================
\begin{frame}[fragile]
  \frametitle{Sage-Grouse}

<<fit-female-trace, echo = FALSE, fig.lp = 'fig:', fig.width = 5, fig.height = 3.5, fig.align = 'center', fig.pos = 'H', message = FALSE>>=
ggs_traceplot(ggs(female_model$mcmc))
@

\end{frame}

%====================================================================
% Section 5
%====================================================================
\section{Conclusion}

%====================================================================
% Frame 5.1
%====================================================================
\begin{frame}[fragile]
  \frametitle{bsm}
  \textbf{bsm} is being developed to 
  \begin{itemize}
  \item be user friendly
  \item use a \textbf{survival}-like syntax
  \item provide a variety of models
  \item provide a variety of survival plots
  \item use modern Bayesian tools
  \end{itemize} \pause
  Current status: proof of concept
\end{frame}

%====================================================================
% Frame 5.2
%====================================================================
\begin{frame}[fragile]
  \frametitle{Future Versions}
  \begin{itemize}
  \item Survival plots
  \item Parametric models
  \item Semi-parametric models with smoothed hazards
  \item Joint models
  \item Improved everything
  \end{itemize}
\end{frame}

%====================================================================
% Frame 6.0
%====================================================================
\plain{Questions?}

%====================================================================
% Frame 7.0
%====================================================================
\begin{frame}[allowframebreaks]

  \frametitle{References}

  \bibliography{bibliography}
  \bibliographystyle{abbrv}

\end{frame}

\end{document}
